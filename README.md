# C# Client Portal Connection Example

A C# implementation of how to connect to the One Exchange Client Portal 

## Installation

1. Install Microsoft Visual Studio
2. Clone repository
3. Open csharpcpconnectionexample folder
4. Click on CSharpCPConnectionExample.sln
5. replace domain, username and password with values provided by One Exchange in the Progrm.cs file.



## Usage

In Visual Studio, click the Run button or F5 to start the program.