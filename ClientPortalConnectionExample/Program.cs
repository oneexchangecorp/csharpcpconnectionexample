﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using WebSocketSharp;

namespace CSharpCPConnectionExample
{


    class Program
    {


        const string domain = "REPLACE WITH CLIENT PORTAL API DOMAIN";
        const string username = "REPLACE WITH USERNAME";
        const string password = "REPLACE WITH PASSWORD";


        private static readonly HttpClient client = new HttpClient();


        public static string AuthToken = "";

        static void Main(string[] args)
        {

            authAndConnect();
            Console.Read();
        }

        static async void authAndConnect()
        {

            var values = new Dictionary<string, string>
            {
               { "username", username },
               { "password", password }
            };

            var content = new FormUrlEncodedContent(values);

            try
            {
                var response = await client.PostAsync("https://" + domain + "/api/login", content);

                var responseString = await response.Content.ReadAsStringAsync();

                IDictionary<string, string> tokenObject = JsonConvert.DeserializeObject<IDictionary<string, string>>(responseString);

                string result;
                if (tokenObject.TryGetValue("token", out result))
                {
                    Console.WriteLine("Found Key");
                    Console.WriteLine(result);

                    AuthToken = result;

                    connectWebsocket();

                    getConfirmsByDateRange();

                    getSettlesbyDate();

                    getIndicesbyDate();

                }
                else
                {
                    Console.WriteLine("Could not find the specified key.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                reconnect();
            }

        }

        static void connectWebsocket()
        {
            WebSocket ws;

            try
            {
                ws = new WebSocket("wss://" + domain);
                ws.SetCookie(new WebSocketSharp.Net.Cookie("token", AuthToken));
                ws.OnOpen += OnOpen;
                ws.OnMessage += OnMessage;
                ws.OnError += OnError;
                ws.OnClose += OnClose;

                ws.Connect();
            }
            catch (Exception ex)
            {
                Console.WriteLine("*************** Websocket FAILED ************************" + ex);

            }

        }

        static private void OnOpen(object sender, EventArgs e)
        {
            Console.WriteLine("*************** Websocket OnOpen ************************");
        }

        static private void OnMessage(object sender, MessageEventArgs e)
        {
            Console.WriteLine("OnMessage: " + e.Data);
        }

        static private void OnError(object sender, ErrorEventArgs e)
        {
            Console.WriteLine("OnError: " + e.ToString());
        }
        static private void OnClose(object sender, EventArgs e)
        {
            Console.WriteLine("OnClose: " + e.ToString());

            reconnect();

        }

        static private void reconnect()
        {
            Random rnd = new Random();
            Thread.Sleep(rnd.Next(1000, 5000));
            authAndConnect();
        }



        //API CALLS

        static async void getConfirmsByDateRange()
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken);
            var response = client.GetAsync("https://" + domain + "/api/v1/confirms?from_date=02/07/2020&to_date=02/07/2020");

            Console.WriteLine("*************** getConfirmsByDateRange ************************");

            var responseString = await response.Result.Content.ReadAsStringAsync();
            Console.WriteLine(responseString);

            Console.WriteLine("*************** End getConfirmsByDateRange ************************");
        }

        static async void getSettlesbyDate()
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken);


            var response = client.GetAsync("https://" + domain + "/api/v1/settles?settle_date=06/17/2020");

            Console.WriteLine("*************** getSettlesbyDate ************************");

            var responseString = await response.Result.Content.ReadAsStringAsync();

            Console.WriteLine(responseString);

            Console.WriteLine("*************** End getSettlesbyDate ************************");
        }

        static async void getIndicesbyDate()
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthToken);

            // date format MM/DD/YYYY
            var response = client.GetAsync("https://" + domain + "/api/v1/cci_index?date=05/06/2020");

            Console.WriteLine("*************** getIndicesbyDate ************************");

            var responseString = await response.Result.Content.ReadAsStringAsync();

            Console.WriteLine(responseString);

            Console.WriteLine("*************** End getIndicesbyDate ************************");
        }


    }
}
